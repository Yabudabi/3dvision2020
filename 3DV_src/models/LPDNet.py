

import torch
import torch.nn as nn
import torch.nn.functional as F

from models.PointNetVlad import STN3d, NetVLADLoupe

# === Adopted PointNet-Part of the PointNetVLAD === 
# due to    - split/concat of input-cloud for the feature network
#           - required parallel output for the graph-based neighborhood aggregeation module:  feature vector f_T and feature_RT after applying
#           the feature transformation
class PointNetfeat(nn.Module):
    def __init__(self, num_points=4096):
        super(PointNetfeat, self).__init__()
        self.stn = STN3d(num_points=num_points, k=3, use_bn=False)
        self.feature_trans = STN3d(num_points=num_points, k=64, use_bn=False)
        self.conv1 = torch.nn.Conv2d(1, 64, (1, 13))
        self.conv2 = torch.nn.Conv2d(64, 64, (1, 1))
        self.bn1 = nn.BatchNorm2d(64)
        self.bn2 = nn.BatchNorm2d(64)
        self.num_points = num_points

    # Assumption: num_dim = 13 as the the local features have been preprocessed (as in the original)
    def forward(self, x):
        """ Forward-Pass of PointNet-Part in the LPD-structure 
        
        Inputs:
            x   tensor, shape=(batch_num_of_queries*num_of_queries, 1, num_points, num_dim)

        Outputs:
            pointfeat               tensor, pcl before feature-trafo, shape=(batch_num_of_queries*num_of_queries, 64, num_points, 1)
            pointfeat_transformed   tensor, pcl after feature-trafo,  shape=(batch_num_of_queries*num_of_queries, num_points, 64)
            point_cloud             tensor, raw input input pcl, shape=(batch_num_of_queries*num_of_queries, 1, num_points, 3))
        """
        batchsize = x.size()[0]
        
        x = x.view(-1, 1, self.num_points, 13)                  # added due to applying the Input-Transformation only on the raw points
        point_cloud, features_cloud = torch.split(x, [3,10], dim=3)

        # --- input transformation + concatination with local features ---
        trans = self.stn(point_cloud)
        x = torch.matmul(torch.squeeze(point_cloud), trans)
        x = x.view(batchsize, 1, -1, 3)

        x = torch.cat((x, features_cloud), dim=3)   # added due to required concatenation of points and local features
        x = x.view(batchsize, 1, -1, 13)
        # --> x : batchsize, 1, 4096, 13
                
        # --- shared mpl (64,64) ---
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        pointfeat = x
        # --> pointfeat : batchsize, 64, 4096, 1 ; input to the feature-transform block of the LPD-Net

        # --- feature transformation ---
        f_trans = self.feature_trans(x)     # f_trans: batch_size x 64 x 64
        x = torch.squeeze(x)                # change shape to batchsize x 64 x 4096
        if batchsize == 1:
            x = torch.unsqueeze(x, 0)
        x = torch.matmul(x.transpose(1, 2), f_trans)  # apply feature-trafo  
        #x = x.transpose(1, 2).contiguous()
        #x = x.view(batchsize, 64, -1, 1)              
        pointfeat_transformed = x
        # ---> pointfeat_transformed : batchsize, 64, 4096, 1 ; point-cloud after Transform-Net --> feature-neighbor relations

        return pointfeat, pointfeat_transformed, point_cloud

class GraphNeighborhoodAggr(nn.Module):
    def __init__(self, input_channels=64):
        super(GraphNeighborhoodAggr, self).__init__()
        self.conv1 = torch.nn.Conv2d(input_channels, 64, (1, 1))
        self.conv2 = torch.nn.Conv2d(64, 64, (1, 1))
        self.bn1 = nn.BatchNorm2d(64)
        self.bn2 = nn.BatchNorm2d(64)


    def forward(self, x):
        # update neighbor relations via MLP(64,64) and aggregeate edge information using max-pooling
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        x = x.max(dim=-1, keepdim=False)[0]        
        return x

    def pairwise_distance(self, x):
        """ Pairwise (euclidean distance) of point cloud

        Input:
            x   tensor (batch_size, num_points, dim)
        Returns:
            pairwise distance: tensor(batch_size, num_points, num_points)
        """
        inner = torch.matmul(x, x.transpose(2, 1))
        squared = torch.sum(x**2, dim=-1, keepdim=True)

        # follows from X^2 + (X^T)^2 - 2*XX^T
        return squared + squared.transpose(2,1) - 2*inner

    def knn(self, x, k):
        """ Computes k nearest neighbors based on the pairwise distance

        Inputs:
            x   tensor (batch_size, num_points, dim)
            k   int
        Outputs:
            nearest neighbors   tensor (batch_size, num_points, k)
        """
        distances = self.pairwise_distance(x)
        return torch.topk(input=distances, k=k, dim=-1, largest=False, sorted=True)[1]

    def compute_edge_features(self, x, nn_idx, k, onlyEdges=False):
        """ Computes edge feature of each point in given pcl

        Inputs:
            x       tensor  (batch_size, num_dim, num_points, 1)
            nn_idx  tensor  nn indicies, (batch_size, num_points, k)
            k       int     number of nearest neighbors
        Outputs:
            edge features   tensor (batch_size, num_points, k, 2*num_dims)
        """
        batch_size = x.size()[0]
        num_points = x.size()[2]
        x = x.view(batch_size, num_points, -1)  # (batch_size, num_points, num_dim)
        
        # define k nearest neighbor indicies as joined indexed 
        idx_base = torch.arange(0, batch_size).view(-1, 1, 1)*num_points    # define the point-[0] idx of each pcl when jointly indexed
        idx = nn_idx + idx_base
        # --> index array (batch_size, num_points, k)) 

        idx = idx.view(-1)      # flatten idx array --> select neighboring points
        _, _, num_dims = x.size()

        point_cloud_neighbors = x.view(batch_size*num_points, -1)[idx, :]   # gain points from flatten idx array
        point_cloud_neighbors = point_cloud_neighbors.view(batch_size, num_points, k, num_dims) 

        # expand pcl in 1 dim and replicate the tensor among those k times --> batch x points x dim --> batch x points x k x dim
        x = x.view(batch_size, num_points, 1, num_dims).repeat(1, 1, k, 1)
    
        # concatente edge feature and central point cloud
        if not onlyEdges:
            edge_features = torch.cat((point_cloud_neighbors-x,x), dim=3).permute(0, 3, 1, 2).contiguous()
        else:
            edge_features = (point_cloud_neighbors-x).permute(0, 3, 1, 2).contiguous()
        return edge_features  


class LPDNet(nn.Module):
    def __init__(self, num_points=4096, output_dim=1024, use_neighborhood_aggr = True):
        super(LPDNet, self).__init__()
        self.pointnet = PointNetfeat(num_points=num_points)
        self.net_vlad = NetVLADLoupe(feature_size=1024, max_samples=num_points, cluster_size=64, output_dim=output_dim, 
									 gating=True, add_batch_norm=True,is_training=True)
        self.dgcnn1 = GraphNeighborhoodAggr(input_channels=128)
        self.dgcnn2 = GraphNeighborhoodAggr(input_channels=64)
        self.conv1 = torch.nn.Conv2d(64, 64, (1, 1))            # conv1-conv3 and bn1 - bn3 from PointNetStructure
        self.conv2 = torch.nn.Conv2d(64, 128, (1, 1))
        self.conv3 = torch.nn.Conv2d(128, 1024, (1, 1))        
        self.bn1 = nn.BatchNorm2d(64)
        self.bn2 = nn.BatchNorm2d(128)
        self.bn3 = nn.BatchNorm2d(1024)
        self.use_Aggregation = use_neighborhood_aggr
        
    def forward(self, x):
        
        batch_size = x.size()[0]
        num_points = x.size()[2]

        # --- Adapted PointNet ----
        x, pointfeat_transformed, point_cloud = self.pointnet(x)
        # Note: x equals the current network in the forward pass: batch x 64 x num_points x 1

        # ---- Graph-based neighborhood aggregation ---
        if self.use_Aggregation:
            k = 20

            # -- KNN-mlp in feature space --
            nn_idx = self.dgcnn1.knn(pointfeat_transformed, k)
            edge_features = self.dgcnn1.compute_edge_features(x, nn_idx, k, False)
            x = self.dgcnn1(edge_features)
            # x -> pointcloud after DG: batch x 64 x num_points

            # -- KNN-mlp in cartesian space --
            # point_cloud = point_cloud.view(-1, num_points, 3)
            # nn_idx = self.dgcnn2.knn(point_cloud, k)

            # x = x.view(batch_size, 64, -1, 1)
            # edge_features = self.dgcnn2.compute_edge_features(x, nn_idx, k, True)
            # x = self.dgcnn2(edge_features)
            # x -> pointcloud after SN: batch x 64 x num_points

            x = x.view(batch_size, 64, -1, 1)
        else:
            x = pointfeat_transformed.transpose(1, 2).contiguous()
            x = x.view(batch_size, 64, -1, 1)              

        # ---- MLP for fusion --- (taken from PointNetVlad after the feature transform)
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        x = self.bn3(self.conv3(x))

        # ---- Apply NetVLad ----
        x = self.net_vlad(x)
        return x


if __name__ == '__main__':

    # --- Simulate Network ---
    num_points = 4096
    num_dim    = 13
    sim_data = torch.rand(10, 1, num_points, num_dim)
    
    model = LPDNet(num_points=4096, output_dim=256)

    model.train()   # set tobe in training mode
    output = model(sim_data)

    print('Model output', output.size())
