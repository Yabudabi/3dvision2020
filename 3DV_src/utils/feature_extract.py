#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 16:18:53 2020
Updated on Sun May 10 09:34:00 2020

Ready for oxford dataset benchmark

@author: Tim Liechti
"""
#------------------------------------------------------------------------------
import numpy as np
from numpy import linalg as la

##BASE_DIR = os.path.dirname(os.path.abspath(__file__))
#base_path = "../../TESTING/CreateNeighborhood/benchmark_datasets/"
#runs_folder="oxford/"
#filename = "point_neighborhoods_20m_10overlap.csv"
#pointcloud_fols="pointcloud_25m_10/"
#neighborhood_fols="KNN_neighborhood_25m_10/"
# -----------------------------------------------------------------------------
#def get_neighborhood():
#	# get neighborhood from file
#	# return it as a nx3 array
#
#	return neighborhood
#------------------------------------------------------------------------------
def get_eigen(neighborhood):
	"""
		Calculate the 3D eigenvalues and eigenvectors and return them

		# neighborhood is a nx3 vector
		# mcovar is a 3x3 covariance matrix
	"""
	mcovar = np.cov(np.transpose(neighborhood),rowvar=False)
	eigenval, eigenvec = la.eig(mcovar)

	return eigenval, eigenvec
#------------------------------------------------------------------------------
def get_eigen2D(neighborhood):
	"""
		c

		# neighborhood is a nx3 vector
		# mcovar is a 3x3 covariance matrix
	"""

	mcovar = np.cov(np.transpose(neighborhood),rowvar=False)
	eigenval2D, eigenvec2D = la.eig(mcovar[:,:2,:2])

	return eigenval2D, eigenvec2D
#------------------------------------------------------------------------------
def get_entropy(neighborhood):
	"""
		Calculate the eigenentropy of the neighborhood and return it
	"""
	# neighborhood needs to be an np.array
	eigenval, eigenvec = get_eigen(neighborhood)
	#
	lam3d1 = eigenval[0]
	lam3d2 = eigenval[1]
	lam3d3 = eigenval[2]

	Sig = lam3d1+lam3d2+lam3d3

	e1 = (lam3d1/Sig)+0.000000001
	e2 = (lam3d2/Sig)+0.000000001
	e3 = (lam3d3/Sig)+0.000000001

	# entropy = Eigenentropy
	entropy = -e1*np.log(e1)-e2*np.log(e2)-e3*np.log(e3)
	return entropy
#------------------------------------------------------------------------------
def local_feature_extraction(pointcloud, neighborhoodID):
	print("INFO: Extracting local features: ...")
	"""
		Calculate the local features and return them in an array
		Point neighborhoods were saved in seperate files in the
		pre-processing step. (run preprocessing_neighborhood.py)

		for a point in the point cloud
		# load neightborhood index
		# calculate features
		# aggregate features in array
	"""
	# pass on featrue array
	indices = neighborhoodID[1:]
	neighborhood = pointcloud[indices,:]
	k_opt = neighborhoodID[0]

	# --- 3D Eigenvalues & Eigenvectors ---
	eigenval, eigenvec = get_eigen(neighborhood)
	#
	lam3D1 = eigenval[0]
	lam3D2 = eigenval[1]
	lam3D3 = eigenval[2]
	# Sum of eigenvalues
	Sig = lam3D1+lam3D2+lam3D3
	e1 = (lam3D1/Sig)+0.000000001
	e2 = (lam3D2/Sig)+0.000000001
	e3 = (lam3D3/Sig)+0.000000001

	# --- F3D features ---
	# Linearity
	L = (e1-e2)/e1
	# Planarity
	#P = (e2-e3)/e1
	# Scattering
	#S = e3/e1
	# Change of curvature
	C = e3/(e1+e2+e3)
	# Omnivariance
	O = np.cbrt(e1*e2*e3)
	# Anisotropy
	#A = (e1-e3)/e1
	# Eigenentropy
	E = -e1*np.log(e1)-e2*np.log(e2)-e3*np.log(e3)
	# Local point density
	D = k_opt/(4/3*(lam3D1*lam3D2*lam3D3))

	# --- 2D Eigenvalues & Eigenvectors ---
	eigenval2D, eigenvec2D = get_eigen2D(neighborhood)
	lam2D1 = eigenval2D[0]
	lam2D2 = eigenval2D[1]
	z_vals = neighborhood[:,2]
	print("z_vals: {}".format(z_vals))

	# --- F2D features ---
	# 2D linearity
	L2D = lam2D1+lam2D2
	# 2D scattering
	S2D = lam2D1/lam2D2
	# --- FV features ---
	# Vertical componenf of normal vector
	vert = eigenvec[2]
	# --- FZ features ---
	# Maximum height difference
	deltaZmax = np.max(z_vals - z_vals.min())
	# Maximum height variance
	varZmax = np.var(z_vals - z_vals.min())


	#features = np.array([L, P, S, C, O, A, E, D, Sig, L2D])#, S2D, vert, deltaZmax, varZmax])
	features = np.array([L, C, O, E, D, L2D, S2D, vert, deltaZmax, varZmax])
	print("done!")

	return features
#------------------------------------------------------------------------------