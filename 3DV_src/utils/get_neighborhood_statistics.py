#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 15:22:27 2020
Updated on Sun May 10 19:43:00 2020
Updated on Tue May 12 06:24:00 2020
Updated on Sat May 16 11:41:00 2020

Ready for oxford dataset benchmark

@author: Tim Liechti
"""
#------------------------------------------------------------------------------
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

base_path = "../../3DV_data/benchmark_datasets/"
filename = "pointcloud_locations_20m_10overlap.csv"
#------------------------------------------------------------------------------
def get_neighborhood_stats(a, b, c, SPACE, runs_folder):
	"""
	"""

	if SPACE == 'lin':
		neighborhood_fols="KNN_neighborhood/"
	elif SPACE == 'log':
		neighborhood_fols="KNN_neighborhood_log/"
	# LINEAR SPACING
	if SPACE == 'lin':
		min_k = a
		max_k = b
		step_k = c
		k_dist = np.linspace(min_k, max_k, num=step_k).astype(int)
		k_div = max_k + 1
	# LOGARITHMIC SPACING
	elif SPACE == 'log':
		min_k = a
		k_log_dist = b
		k_log_dist_max = k_log_dist
		k_div = k_log_dist_max + min_k + 1
	else:
		print("Specify global variable: 'SPACE'")

	all_folders=sorted(os.listdir(os.path.join(base_path,runs_folder)))
	if ".DS_Store" in all_folders:
		all_folders.remove(".DS_Store")
	cloud_values=list()
	# iterating over folders --> scenes, [0:1] for testing
	for folder in all_folders:
		df_locations=pd.read_csv(os.path.join(base_path,runs_folder,folder,filename),sep=',')
		fromfile_locations=runs_folder+folder+'/'+neighborhood_fols+df_locations['timestamp'].astype(str)+'.bin'
		neighborhood_files = fromfile_locations.tolist()
		point_values=list()
		#print("-----------------------")
		# iterating over files --> point clouds, [0:3] for testing
		for file in neighborhood_files:
			fromfilename=os.path.join(base_path,file)
			neighborhood_data = np.fromfile(fromfilename,dtype=np.int,sep=',')
			neighborhood_data = np.reshape(neighborhood_data,(neighborhood_data.shape[0]//k_div,k_div))
			values = neighborhood_data[:,0]
			point_values.append(values)

		cloud_values.append(point_values)

	value_array = np.asarray(cloud_values)
	data = value_array
	all_data = data.flatten()

	# LINEAR SPACING
	if SPACE == 'lin':
		plt.hist(all_data, bins='auto', alpha=0.5)
		plt.yscale('log')
		plt.xlabel('Optimal neighborhood size')
		plt.ylabel('Occurrence')
	# LOGARITHMIC SPACING
	elif SPACE == 'log':
		plt.hist(all_data, bins=k_log_dist, alpha=0.5)
		plt.yscale('log')
		plt.xscale('log')
		plt.xlabel('Optimal neighborhood size')
		plt.ylabel('Occurrence')
	else:
		print("Specify global variable: 'SPACE'")

#------------------------------------------------------------------------------
def main():

	min_k = 20
	min_k_log = 0.3
	max_k_log = 2.7
	step_k = 10

	k_log_dist = np.logspace(min_k_log, max_k_log, step_k, endpoint=True, base=10)
	k_log_dist = k_log_dist.astype(int)
#	get_neighborhood_stats(min_k, k_log_dist)

#------------------------------------------------------------------------------
if __name__ == '__main__':
	main()