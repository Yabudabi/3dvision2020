#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 16 08:53:52 2020
Updated on Sun May 10 19:43:00 2020
Updated on Mon May 11 10:22:00 2020
Updated on Tue May 12 06:23:00 2020
Updated on Wed May 13 09:00:00 2020
Updated on Wed May 13 18:19:00 2020
Updated on Fry May 15 09:01:00 2020 - Multiprocessing added
Updated on Sat May 16 11:40:00 2020

Ready for oxford dataset benchmark

@author: Tim Liechti
"""
# --- INFO ---
# adjust base_path if necessary
# for linear spacing of k, set SPACE = 'lin'
# for logarithmic spacing of k, set SPACE = 'log'
#------------------------------------------------------------------------------
import os
import numpy as np
import pandas as pd
import time
import multiprocessing
from sklearn.neighbors import NearestNeighbors
from feature_extract import get_entropy
from get_neighborhood_statistics import get_neighborhood_stats
#&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

#--- Select dataset ---
runs_folder="oxford/"

#&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

#--- Select spacing ---
#SPACE = 'lin'
SPACE = 'log'

#&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
# LINEAR SPACING
if SPACE == 'lin':
	MIN_K = 20
	MAX_K = 100
	STEP_K = 10
# LOGARITHMIC SPACING
elif SPACE == 'log':
	MIN_K = 20
	MIN_K_LOG = 0.3
	MAX_K_LOG = 2.603
	STEP_K_LOG = 10
#------------------------------------------------------------------------------
base_path = "../../3DV_data/benchmark_datasets/"
filename = "pointcloud_locations_20m_10overlap.csv"
pointcloud_fols="pointcloud_20m_10overlap/"
# LINEAR SPACING
if SPACE == 'lin':
	neighborhood_fols="KNN_neighborhood/"
# LOGARITHMIC SPACING
elif SPACE == 'log':
	neighborhood_fols="KNN_neighborhood_log/"
#------------------------------------------------------------------------------
def get_opt_neighborhood(pointcloud):
	"""
		Define neighborhood sizes to try

		# iterate through pointcloud
		# for every point do
			# knn with different k (neighborhood size)
			# pass point cloud to get_entropy to calculate entropy
			# pick k with lowest entropy
			# return data vector
	"""
	# LINEAR SPACING
	if SPACE == 'lin':
		min_k = MIN_K
		max_k = MAX_K
		step_k = STEP_K
		delta_k = int((max_k - min_k)/step_k)
	# LOGARITHMIC SPACING
	elif SPACE == 'log':
		min_k = MIN_K
		min_k_log = MIN_K_LOG
		max_k_log = MAX_K_LOG
		step_k = STEP_K_LOG
		k_log_dist = np.logspace(min_k_log, max_k_log, step_k, endpoint=True, base=10)
		k_log_dist = k_log_dist.astype(int)
		#print(k_log_dist)
	else:
		print("Specify global variable: 'SPACE'")
	# pointcloud has to be np.array() from here on
	data_vec = list()
	for line in range(0,np.size(pointcloud,0)-1): # 1000 for testing np.size(pointcloud,0)-1)
		point = [pointcloud[line,:]]
		point = np.asarray(point)

		entropy = list()
		ind_vec = list()
		for steps in range(1, step_k):
			# LINEAR SPACING
			if SPACE == 'lin':
				k = min_k + (steps-1)*delta_k
			# LOGARITHMIC SPACING
			elif SPACE == 'log':
				k = min_k + k_log_dist[steps-1]
			else:
				print("Specify global variable: 'SPACE'")

			# calculating point neighborhood for different neighborhood sizes
			neighbors = NearestNeighbors(n_neighbors=k)
			neighbors.fit(pointcloud)
			distance, index = neighbors.kneighbors(point)

			# LINEAR SPACING
			if SPACE == 'lin':
				fill = np.zeros((1,max_k-k),dtype=int)
			# LOGARITHMIC SPACING
			elif SPACE == 'log':
				fill = np.zeros((1,min_k+k_log_dist[-1]-k),dtype=int)
			else:
				print("Specify global variable: 'SPACE'")

			index = np.concatenate((index, fill), axis=None)
			ind_vec.append(index)

			neighbor_points = pointcloud[index[0:k].astype(int)]
			selected_points = np.reshape(neighbor_points,(k,3))

			# get entropy and write to array
			entropy_value = get_entropy(selected_points)
			entropy.append(entropy_value)

		# ---- delete first row of indvec
		ind_v = np.asarray(ind_vec)
		# find min entropy
		entropy = np.asarray(entropy)
		# index of the lowest entropy value
		k_ind = np.argmin(entropy)

		# number of neighbors that minimizes the entropy
		# LINEAR SPACING
		if SPACE == 'lin':
			k_opt = min_k + (k_ind + 1)*delta_k
		# LOGARITHMIC SPACING
		elif SPACE == 'log':
			k_opt = min_k + k_log_dist[k_ind]
		else:
			print("Specify global variable: 'SPACE'")
		# indices of neighboring points
		opt_ind = ind_v[k_ind,:]
		# write neighborhood size in front of indices
		line_vec = np.concatenate((k_opt, opt_ind), axis=None)
		# list k_okt and indices for every point in cloud
		data_vec.append(line_vec)

	data_vec = np.asarray(data_vec)
	#data_vec.tofile("data_vec.txt",sep=',')
	#print(data_vec)
	return data_vec
#------------------------------------------------------------------------------
def processing(fromfilename, tofilename):
	"""
		Skip processing if file already exists
		Call get_opt_neigborhood
		Write neighborhood data to file for later access
	"""
	if not os.path.exists(tofilename):
		# get pointcloud
		pointcloud = np.fromfile(fromfilename,dtype=np.float64)
		pointcloud = np.reshape(pointcloud,(pointcloud.shape[0]//3,3))
		# get optimal neighborhood and its size
		neighborhood_data = get_opt_neighborhood(pointcloud)
		#print(neighborhood_data)
		neighborhood_data.tofile(tofilename,sep=',')

	else:
		print('File {} already exists. Skip this file.'.format(tofilename))
#------------------------------------------------------------------------------
def multiprocessing_func(inputlist):
	"""
		Call processing and pass on path names
	"""
	fromfilename = inputlist[0]
	tofilename = inputlist[1]
	processing(fromfilename, tofilename)
#------------------------------------------------------------------------------
def multiprocessing_prep(inputlist):
	"""
		Prepare multiprocessing pool
		Call multiprocessing_func
	"""
	pool = multiprocessing.Pool()
	pool.map(multiprocessing_func, inputlist)
	pool.close()
#------------------------------------------------------------------------------
def process_pointclouds():
	"""
		Create list with folders to process
		Create list of files for each folder
		Create job list for multiprocessing with
			each file being processed in a job.
		Call multiprocessing_prep
	"""

	# list folders in directory
	all_folders=sorted(os.listdir(os.path.join(base_path,runs_folder)))
	if ".DS_Store" in all_folders:
		all_folders.remove(".DS_Store")
	for folder in all_folders:
		#print("------------------------------------------")
		print("INFO: Processing folder: {} ...".format(folder))
		df_locations= pd.read_csv(os.path.join(base_path,runs_folder,folder,filename),sep=',')
		tofile_locations=runs_folder+folder+'/'+neighborhood_fols+df_locations['timestamp'].astype(str)+'.bin'
		df_locations['timestamp']=runs_folder+folder+'/'+pointcloud_fols+df_locations['timestamp'].astype(str)+'.bin'
		df_locations=df_locations.rename(columns={'timestamp':'file'})
		# create neighborhood data folder
		neighborhood_path = os.path.join(base_path, runs_folder, folder, neighborhood_fols)
		if not os.path.exists(neighborhood_path):
			   os.mkdir(neighborhood_path)
		# list files in folder
		pointcloud_files = df_locations['file'].tolist()
		neighborhood_files = tofile_locations.tolist()

		it = 0
		inputlist = []
		for file in pointcloud_files: #pointcloud_files[0:3] for testing
			inp = [os.path.join(base_path,file), os.path.join(base_path,neighborhood_files[it])]
			inputlist.append(inp)
			it = it+1
		multiprocessing_prep(inputlist)

		print("done!")

#------------------------------------------------------------------------------
def main():

	starttime = time.time()
	print('{} CPUs available for multiprocessing'.format(multiprocessing.cpu_count()))
	process_pointclouds()
	print('INFO: Computation time: {}'.format(time.time() - starttime))

	if SPACE == 'lin':
		min_k = MIN_K
		max_k = MAX_K
		step_k = STEP_K
		get_neighborhood_stats(min_k, max_k, step_k, SPACE, runs_folder)
	# LOGARITHMIC SPACING
	elif SPACE == 'log':
		min_k = MIN_K
		min_k_log = MIN_K_LOG
		max_k_log = MAX_K_LOG
		step_k = STEP_K_LOG
		k_log_dist = np.logspace(min_k_log, max_k_log, step_k, endpoint=True, base=10)
		k_log_dist = k_log_dist.astype(int)
		get_neighborhood_stats(min_k, k_log_dist[-1], [], SPACE, runs_folder)
	else:
		print("Specify global variable: 'SPACE'")

#------------------------------------------------------------------------------
if __name__ == '__main__':
	main()
#------------------------------------------------------------------------------
# find . -name ".DS_Store" -delete