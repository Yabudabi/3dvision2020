The Oxford data set has to be downloaded to this folder. Currently only a very small fraction of the oxford data set is available that can be used 
for sanity checks on the code.
The benchmark datasets introduced in the original work can be downloaded [here](https://drive.google.com/open?id=1H9Ep76l8KkUpwILY-13owsEMbVCYTmyx).
Those are from the open-source [Oxford RobotCar](https://robotcar-dataset.robots.ox.ac.uk/) for large-scale place recognition.  