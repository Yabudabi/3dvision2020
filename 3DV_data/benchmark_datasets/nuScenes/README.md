This is the Software Development Kit (SDK) for the [nuScenes](https://github.com/nutonomy/nuscenes-devkit).  
The devkit is tested for Python 3.6 and Python 3.7. It can be installed via pip: "pip install nuscenes-devkit"  
  
The benchmark datasets introdruced in this work can be downloaded [here](https://drive.google.com/drive/folders/1d4YFA3bQf9Z1IRvImW4tukCTziVQC2Mb). 
  
- All submaps are in binary file format  
- Ground truth GPS coordinate of the submaps are found in the corresponding csv files for each route  
- Filename of the submaps are their timestamps which is consistent with the timestamps in the csv files  
- Use CSV files to define positive and negative point clouds  
- All samples are preprocessed with the road and scanning car removed and downsampled to 4096 points  

The nuScenes consists of data in four different maps. For this work, we restrict ourself to the data taken in Boston.  
  
- 29 sets in total of full and partial runs  
- Used both full and partial runs for training but only used full runs for testing  
- Training submaps are found in the folder "pc/" and its corresponding csv file is "globalCentroids_boston_seaport.csv"  
- Test submaps found in the folder "pc_test/" and its corresponding csv file is "globalCentroids_test_boston_seaport.csv"  
  
The Matlab script *generate_submap_nuScenes.m* was used for the pre-processing of each sample and for the calculation of the local centroid of the corresponding point cloud.  
The python script *getGlobalCentroids.py* was used for the transformation and the calulcation of the local centroids to the global UTM coordinate frame. In order to run this script, the SDK has to be installed.

**The resulting point clouds are in the folder boston-seaport.**

