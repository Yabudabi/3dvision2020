The boston seaport dataset from nuScenes has to be downloaded to this folder.  
The additional benchmark dataset that we pre-processed for our work can be downloaded [here](https://drive.google.com/drive/folders/1d4YFA3bQf9Z1IRvImW4tukCTziVQC2Mb).
Those are from the open-source [nuScenes](https://www.nuscenes.org/) for large-scale autonomous driving.  
