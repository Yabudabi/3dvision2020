# Loop Closure Detection on Point Clouds - 3D Vision
This project was carried out as a part of the lecture "3D Vision" taught at ETH Zurich in 2020.  
This README gives a short overview of the project, lists and explains all the files and scripts that we have written by our own or adapted from others.

## Introduction

[PointNetVLAD](https://arxiv.org/abs/1804.03492) and [LPD-Net](https://arxiv.org/abs/1812.07050) are deep networks that address the problem of large-scale place recognition through retrieval from raw 3D point clouds.  
A main part of our project is to implement this LPD-Net in pyTorch based on the original work of PointNetVLAD [here](https://github.com/mikacuy/pointnetvlad).  
Please refer to this repository when running the PointNetVLAD on the Oxford RobotCar data set.  

## Benchmark Data sets
The benchmark data sets introduced in the original work can be downloaded [here](https://drive.google.com/open?id=1H9Ep76l8KkUpwILY-13owsEMbVCYTmyx).
Those are from the open-source [Oxford RobotCar](https://robotcar-dataset.robots.ox.ac.uk/) for large-scale place recognition.  
The additional benchmark data sets that we pre-processed and use in our work can be downloaded [here](https://drive.google.com/drive/folders/1d4YFA3bQf9Z1IRvImW4tukCTziVQC2Mb).
The original data set is from the open-source [nuScenes](https://www.nuscenes.org/) for large-scale autonomous driving.  
Its README.md is in the folder /3dvision2020/3DV_data/benchmark_datasets/nuScenes and should be studied when applying this data set.

## Project Code
Code was tested using Python 3 and PyTorch 1.4 with CUDA 10.  
All other libraries used are standard Python libraries as numpy, pandas, sklearn, etc.  

## Structure of the project
The structure of this repository is as follows: 
  
- 3DV_data  
	- benchmark_datasets  
		- oxford:  
		Save the data set of the Oxford RobotCar in this folder.
		- nuScenes:  
		Save the data set of the nuScenes in the folder boston-seaport/.
			- generate_submaps_nuScenes
			MATLAB script that allows to generate and pre-process the nuScenes data set in a similar way as they did for the Oxford RobotCar data set.  
			Based on the PointNetVLAD repository with adaptations from us.
			- getGlobalCentroids  
			Python script that allows to calculate the global centroids for each point cloud.  
			Written by us.

- 3DV_src
	- generating_queries    
	Python scripts that compute the tuples for each the training and testing set, respectively. 
	From PointNetVLAD with some adaptions for the nuScenes data set.
	- utils
		- feature_extract.py  
		Python script that extracts the local features and returns them in an array.
		- get_neighborhood_statistics.py  
		Python script that returns a histogram of the frequency of the optimal neighborhood sizes.
		- loading_pointclouds.py  
		Python script containing functions that are used for loading the pointclouds during training and loading the pre-processed neighborhoods from files.
		- preprocess_neighborhood.py  
		Python script that calculates optimal neighborhood size found by comparing linearly growing sizes.
		- preprocess_neighborhood_log.py  
		Python script that calculates optimal neighborhood size found by comparing logarithmically growing sizes.
	- models  
		- LPDNet.py  
		Our implementation of the LPD-Net, based on the PointNetVlad.py
		- PointNetVlad.py
	- loss.py  
		Contains the definitions for the loss function used in the training process.
	- train.py  
		Run this file to initiate training process.
	- evaluate.py  
		Run this tile to evaluate the trained network on the validation data.


In order to run the LPD-Net on the oxford data set, execute the following steps in order:

	- donwload oxford data set and store the data as indicated by the sample folders 
	
	- run generate_test_sets.py  
	
	- run generate_training_tuples_baseline.py  
	
	- run prepare_data.py  
	
	- run train.py  
	
	- run evaluate.py  